﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace PR.BusinessLogic.Tests
{
    [TestClass()]
    public class ERP_TouchTests
    {
        [TestMethod()]
        public void GetFacilityCapacityTest()
        {
            try
            {
                string conString = ConfigurationManager.AppSettings["ConnectionString"].ToString();

                PR.Capacity.CapacityLogic smd = new PR.Capacity.CapacityLogic(conString);

                //  string testStr = smd.TestMethod();


                int numberOfDays = 60;
                DateTime startDate = Convert.ToDateTime("03/01/2020");
                //DateTime endDate = Convert.ToDateTime("05/01/2020");

                var ds = smd.GetCapacity("L167", startDate, numberOfDays, 0, null, 16);
                ds = smd.GetCapacity("L157", startDate, numberOfDays, 0, null, 16);
                //ds = smd.GetCapacity("L160", startDate, numberOfDays, 0, null, 16);
                //ds = smd.GetCapacity("L001", startDate, numberOfDays, 0, null, 16);

                Assert.Fail();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        [TestMethod()]
        public void CheckPassword()
        {
            try
            {
                // var EnDeKey = "P@ckr@tFac!l1tyApp"; //from config file ( Facility App)
                var EnDeKey = "R@j1b@2013"; //from config file ( Passport)

                var encPwd = "37rBLFP5CKAVg7mMwaGKAQ==";
                var decPwd = PR.UtilityLibrary.CommonUtility.Decrypt(encPwd, EnDeKey);

                //for Testing Purpose only
                encPwd = PR.UtilityLibrary.CommonUtility.Encrypt("tglass@123", EnDeKey);
                decPwd = PR.UtilityLibrary.CommonUtility.Decrypt(encPwd, EnDeKey);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

    }
}