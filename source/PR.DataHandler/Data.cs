using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;


namespace PR.CapacityDataHandler
{
    public static class Data
    {
        public static string _connStringPR = String.Empty;

        #region "Methods"


        public static SqlConnection GetConnection(string connString)
        {
            SqlConnection conn = new SqlConnection(connString);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            return conn;
        }

        public static SqlConnection GetPRConnection()
        {
            SqlConnection conn = new SqlConnection(_connStringPR);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            return conn;
        }

        public static bool CloseConnection(SqlConnection conn)
        {
            try
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                    conn.Dispose();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static void LoadSettings(string conString = "")
        {
            if (_connStringPR == String.Empty)
            {
                //Added by Sohan
                _connStringPR = conString;

                ////Added to remove the C driver text file settings dependency for Neuron ESB usage.

                if (_connStringPR == String.Empty)
                    _connStringPR = ConfigurationManager.AppSettings["ConnectionString"].ToString();

                //Commented by Sohan
                //// this will support for different web app on same server with different SQL server
                //string strFilepath = ConfigurationManager.AppSettings["PRSettingFilePath"];
                //if (string.IsNullOrWhiteSpace(strFilepath))
                //    strFilepath = @"C:\PRSettings\PR.xml";

                //XmlTextReader reader = new XmlTextReader(strFilepath);
                //while (reader.Read())
                //{
                //    if (reader.NodeType == XmlNodeType.Element)
                //    {
                //        if (reader.HasAttributes)
                //        {
                //            reader.MoveToAttribute("key");
                //            switch (reader.Value)
                //            {
                //                case "PRSLocal":
                //                    reader.MoveToAttribute("value");
                //                    _connStringPR = reader.Value;
                //                    break;
                //            }
                //        }
                //    }
                //}
            }
        }


        #endregion


    }
}
