using System;
using System.Data;
using System.Data.SqlClient; 

namespace PR.CapacityDataHandler
{
    public class LocalDataHandler 
    {
        public LocalDataHandler()
        {
            Data.LoadSettings();
        }
        public LocalDataHandler(string conString)
        {
            Data.LoadSettings(conString);
        }

        public void InsertPRErrorLog(DateTime errorDateTime, string userId, string exceptionType, string exceptionMsg, string stackTrace, int qrid)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();

                param[0].ParameterName = "@ErrorDTM";
                param[0].Value = errorDateTime;
                param[1].ParameterName = "@UserId";
                param[1].Value = userId;
                param[2].ParameterName = "@ExceptionType";
                param[2].Value = exceptionType;
                param[3].ParameterName = "@ExceptionMsg";
                param[3].Value = exceptionMsg;
                param[4].ParameterName = "@StackTrace";
                param[4].Value = stackTrace;
                param[5].ParameterName = "@QRID";
                param[5].Value = qrid;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGErrorLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetCapacity(DateTime startDate, DateTime endDate, string locationCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsCapacity = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@StartDate";
                param[0].Value = startDate;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@EndDate";
                param[1].Value = endDate;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@LocationCode";
                param[2].Value = locationCode;

                dsCapacity = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapacity", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsCapacity;
        }

        public DataSet GetCapCategoryTouchTypes()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapCategoryTouchTypes");
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetTouchMilesByTypeByFacilityByDateRange(int touchTypeId, string locationCode, DateTime startDate, DateTime endDate)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@TouchTypeId";
            param[0].Value = touchTypeId;

            param[1] = new SqlParameter();
            param[1].ParameterName = "@locationCode";
            param[1].Value = locationCode;

            param[2] = new SqlParameter();
            param[2].ParameterName = "@StartDate";
            param[2].Value = startDate;

            param[3] = new SqlParameter();
            param[3].ParameterName = "@EndDate";
            param[3].Value = endDate;

            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetTouchMilesByTypeByFacilityBYDateRange", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetAllCapTouchTypes()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetAllTouchTypes");
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetCapacityColorActionsByRole(int? roleLevelId)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@RoleLevelId";
            param[0].Value = roleLevelId;


            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure,
                    "USP_CAP_GetCapacityColorActionsByRole", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public String GetCapCategoryByCategoryId(int categoryId)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@CategoryId";
            param[0].Value = categoryId;
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapCategory", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0)
            {
                if (dsResult.Tables[0].Rows[0][0] != DBNull.Value)
                    return Convert.ToString(dsResult.Tables[0].Rows[0][0]);
                else
                    return null;
            }
            else
                return null;
        }

        public DataSet GetFacilitiesWithLimitedCapacities(string locationCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@LocationCode";
            param[0].Value = locationCode;
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetFacilitiesWithLimitedCapacity", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        //Added by Sohan, Get end Point config. 
        public DataSet GetEsbEndPointConfig()
        {
            //// Get from database later once finalized.
            //return new EsbEndPointConfig
            //{
            //    Url = "http://esb.1800packrat.com:40020/",
            //    Username = "Packrat2019",
            //    Password = "SanFran2005!"
            //};

            Data.LoadSettings();
            DataSet dsConfig = new DataSet();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                dsConfig = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ERP_GetEndPointConfig");
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsConfig;
        }

        public DataSet GetCapacityCalendarData(DataTable capTouchMilesDataTable)
        {
            Data.LoadSettings();
            DataSet dsCapacityData = new DataSet();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[1];
                param[0] = new SqlParameter() { ParameterName = "@CAPTouchMilesDataTable", Value = capTouchMilesDataTable };

                dsCapacityData = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "SP_GetCapacityCalendarData", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsCapacityData;

        }

    }

}
