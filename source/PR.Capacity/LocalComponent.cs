using PR.CapacityDataHandler;
using System;
using System.Data;

namespace PR.Capacity
{
    public class LocalComponent : CapacityLogicBase
    {
        LocalDataHandler handler = null;

        public LocalComponent(string conString)
            : base()
        {
            handler = new LocalDataHandler(conString);
        }

        public DataSet GetFacilitiesWithLimitedCapacities(string locationCode)
        {
            return handler.GetFacilitiesWithLimitedCapacities(locationCode);
        }

        public DataSet GetCapacity(DateTime startDate, DateTime endDate, string locationCode)
        {
            return handler.GetCapacity(startDate, endDate, locationCode);
        }

        public DataSet GetCapCategoryTouchTypes()
        {
            return handler.GetCapCategoryTouchTypes();
        }

        public string GetCapCategoryByCategoryId(int categoryId)
        {
            return handler.GetCapCategoryByCategoryId(categoryId);
        }

        public DataSet GetTouchMilesByTypeByFacilityByDateRange(int touchTypeId, string locationCode, DateTime startDate, DateTime endDate)
        {
            return handler.GetTouchMilesByTypeByFacilityByDateRange(touchTypeId, locationCode, startDate, endDate);
        }

        public DataSet GetAllCapTouchTypes()
        {
            return handler.GetAllCapTouchTypes();
        }

        public DataSet GetCapacityColorActionsByRole(int? roleLevelId)
        {
            return handler.GetCapacityColorActionsByRole(roleLevelId);
        }

        public DataSet GetCapacityCalendarData(DataTable capTouchMilesDataTable)
        {
            return handler.GetCapacityCalendarData(capTouchMilesDataTable);
        }
    }
}
