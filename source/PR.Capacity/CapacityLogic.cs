﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Newtonsoft.Json;
using static PR.Entities.ERP_Enums;
using static PR.Entities.PREnums;
using PR.CapacityDataHandler;

namespace PR.Capacity
{
    public class EsbEndPointConfig
    {
        public string Url { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
    }

    public partial class CapacityLogic : CapacityLogicBase
    {
        LocalDataHandler handler = null;
        public static string _connStringPR = String.Empty;

        public string ConString
        {
            get { return _connStringPR; }
            set { _connStringPR = value; }
        }

        public CapacityLogic(string conString)
        {
            _connStringPR = conString;
            handler = new LocalDataHandler(conString);

            Data.LoadSettings(_connStringPR);
            base.GetEsbEndPintConfig(_connStringPR);
        }

        //public CapacityLogic(string conString)
        //{
        //    handler = new LocalDataHandler(base.ConString);
        //}

        public string TestMethod()
        {
            return "Hi Capacity";
        }

        /// <summary>
        /// Method to get touch available schedule for capacity calculation for particular facility - TG-776.
        /// </summary>
        /// <param name="locationCode"></param>
        /// <param name="startDate"></param>
        /// <param name="numberOfDays"></param>
        /// <returns></returns>
        private DataSet GetTouchAvailableSchedule(string locationCode, DateTime startDate, int numberOfDays)
        {
            DataSet dsSchedule = null;

            try
            {
                object requestObject = new
                {
                    startDate = startDate.ToString("MM/dd/yyyy"),
                    locationCode = locationCode,
                    numberOfDays = numberOfDays,
                    endDate = startDate.AddDays(numberOfDays).ToString("MM/dd/yyyy"),
                };

                dsSchedule = new DataSet();

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB("GetTouchAvailableSchedule", requestJson);

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                {
                    dsSchedule = JsonConvert.DeserializeObject<DataSet>(response);
                    dsSchedule.Tables[0].TableName = "QTs";
                }

            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, "", ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return dsSchedule;
        }

        public DataSet GetCapacity(string locationCode, DateTime startDate, int numberOfDays, decimal distance = 0, int? touchTypeId = 0, int? containerSize = 0)
        {
            //_connStringPR = conString;

            #region Set defaults / Dummy
            if (!touchTypeId.HasValue)
                touchTypeId = 2;

            if (!containerSize.HasValue)
                containerSize = (int)UnitOrContainerSize.EightFeet;
            #endregion

            LocalComponent local = new LocalComponent(_connStringPR);
            ScheduleCalendar scheduleCalendar = new ScheduleCalendar();

            #region SFERP-TODO-ESBMTD

            // Added by Sohan
            // Get Touch Available schdule for Calender/Facility Capacity from Salesforce using ESB
            DataSet dsSchedule = GetTouchAvailableSchedule(locationCode, startDate, numberOfDays);

            #endregion

            // Get all touch types
            var dsTouchTypes = GetAllCapTouchTypes();

            bool overrideTouchSchedule = OverrideTouchScheduling(dsTouchTypes, touchTypeId.Value);

            #region Limited facility - White dates
            DataSet dsLimitedFacility = local.GetFacilitiesWithLimitedCapacities(locationCode);
            int limitDaysTo = dsLimitedFacility.Tables[0].Rows.Count > 0 ? Convert.ToInt32(dsLimitedFacility.Tables[0].Rows[0]["ScheduleBlockDays"]) : 0;
            // CONSIDERING TODAY
            limitDaysTo--;
            #endregion 

            #region Get capacity
            // GET FACILITY ID BY LOCATION CODE

            DataSet dsCapacity = local.GetCapacity(startDate, startDate.AddDays(numberOfDays), locationCode);
            if (dsCapacity == null || dsCapacity.Tables.Count <= 0 || dsCapacity.Tables[0].Rows.Count <= 0)
            {
                throw new Exception("Invalid capacity.");
            }
            DataSet dsCategoryTouchTypes = local.GetCapCategoryTouchTypes();

            // GET CAP CATEGORY TYPE BY TOUCH TYPE
            int CAPCategoryId;
            DataRow drTouchType = dsCategoryTouchTypes.Tables[0].Rows.OfType<DataRow>().FirstOrDefault(c => Convert.ToInt32(c["TouchTypeId"]) == touchTypeId);
            if (drTouchType == null)
                throw new Exception("Invalid Touch type.");
            else
                CAPCategoryId = Convert.ToInt32(drTouchType["CAPCategoryId"]);

            // GET CATEGORY BY CATEGORYID

            string category = local.GetCapCategoryByCategoryId(CAPCategoryId);

            // IDENTITY THE CATEGORY BASED ON THE TOUCH TYPE AND GET ALL ITS TOUCH TYPES BASED ON CATEGORY -  IT IS CONFUSING BUT IT WORKS LIKE THAT
            // AS WE HAVE THE CAP CATEGORY ID ALREADY, WE NEED TO GET ALL THE TOUCH TYPES
            List<int> touchTypeIds = dsCategoryTouchTypes.Tables[0].Rows.OfType<DataRow>().Where(c => Convert.ToInt32(c["CAPCategoryId"]) == CAPCategoryId).Select(c => Convert.ToInt32(c["TouchTypeId"])).ToList();

            // CHECK IF THERE IS ANY ALLOCATION FOR HOLIDAY ON THE GIVEN DATE
            // CASE OF BLUE DATES CONSIDER THE FLAG .STORESTATUS

            // GET TOUCH MILES BASED ON THE TOUCHTYPE/TOUCH TIME
            DataSet dsTouchMiles = local.GetTouchMilesByTypeByFacilityByDateRange(touchTypeId.Value, locationCode, startDate, startDate.AddDays(numberOfDays));

            #endregion

            #region Loop each Schedule day and determine Touch availability for each scheduled date.
            for (DateTime dtSchedule = startDate; dtSchedule < startDate.AddDays(numberOfDays); dtSchedule = dtSchedule.AddDays(1))
            {

                decimal totalDistance = distance;
                decimal touchDistance = dsTouchMiles.Tables[0].Rows.OfType<DataRow>().Where(c => Convert.ToDateTime(c["CapacityDate"]).ToShortDateString() == dtSchedule.ToShortDateString()).Select(c => Convert.ToDecimal(c["TouchMiles"])).FirstOrDefault();

                totalDistance += touchDistance;

                #region Initialize Schedule Calendar tables for Scheduled day
                ScheduleCalendar.AMDataTable amCalendar = scheduleCalendar.AM;
                ScheduleCalendar.PMDataTable pmCalendar = scheduleCalendar.PM;
                ScheduleCalendar.AnyTimeDataTable anytimeCalendar = scheduleCalendar.AnyTime;

                ScheduleCalendar.AMRow amRow = amCalendar.NewAMRow();
                ScheduleCalendar.PMRow pmRow = pmCalendar.NewPMRow();
                ScheduleCalendar.AnyTimeRow anyTimeRow = anytimeCalendar.NewAnyTimeRow();

                amRow.StoreStatus = "";
                pmRow.StoreStatus = "";
                anyTimeRow.StoreStatus = "";

                amRow.TripMiles = totalDistance;
                pmRow.TripMiles = totalDistance;
                anyTimeRow.TripMiles = totalDistance;

                amRow.Date = dtSchedule;
                pmRow.Date = dtSchedule;
                anyTimeRow.Date = dtSchedule;
                #endregion /Initialize Schedule Calendar tables for Scheduled day

                #region Loop through Sitelink touches for scheduled day and accum touch buckets
                int i = 0;

                DataRow[] drTouches = new DataRow[dsSchedule.Tables["QTs"].Rows.Count];

                foreach (DataRow drTemp in dsSchedule.Tables["QTs"].Rows)
                {
                    if (Convert.ToInt32(drTemp["TouchStatusId"]) != Convert.ToInt32(eQTStatus.OnHold) && drTemp["ScheduledDate"] != null && Convert.ToDateTime(drTemp["ScheduledDate"]).ToShortDateString() == dtSchedule.ToShortDateString())
                    {
                        drTouches[i] = drTemp;
                        i++;
                    }
                }

                decimal amTotalDistance = 0M;
                decimal pmTotalDistance = 0M;
                decimal anytimeTotalDistance = 0M;

                // OTHER BOOKINGS USUALLY MEANS THE EITHER SALES/SERVICE. INTIALLY TO START WITH CAPACITY IS FOR SALES/SERVICES
                decimal amOtherTotalDistance = 0M;
                decimal pmOtherTotalDistance = 0M;
                decimal anytimeOtherTotalDistance = 0M;

                foreach (DataRow drTouch in drTouches)
                {
                    if (drTouch != null)
                    {
                        string time = GetTimeSlaught(drTouch["TouchTime"].ToString());
                        // TOUCH HANDLING DISTANCE
                        var touchHandlingDistance = GetTouchMilesByTouchTypeId(Convert.ToInt32(drTouch["TouchTypeId"]), Convert.ToDateTime(drTouch["ScheduledDate"]), locationCode);
                        // IF THE TOUCH TYPE HAS ALLOW SCHEDULING SET TO TRUE THOSE MILEAGE BOOKINGS ARE NOT COUNTED
                        // EXAMPLE WA/IB0/OBO
                        if (touchHandlingDistance <= 0 && OverrideTouchScheduling(dsTouchTypes, Convert.ToInt32(drTouch["TouchTypeId"])))
                            continue;

                        // SUM UP THE MILEAGES ONLY WHEN THEY ALL BELONG TO SAME ALLOCATIONS
                        // THIS IS POSSIBLE BY CHECKING THE CURRENT TOUCH TYPE TO BE IN THE touchTypeIds LIST
                        if (touchTypeIds.Any(c => c == Convert.ToInt32(drTouch["TouchTypeId"])))
                        {
                            if (time == "AM")
                            {
                                if (!OverrideTouchScheduling(dsTouchTypes, Convert.ToInt32(drTouch["TouchTypeId"])))
                                {
                                    amTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);
                                    anytimeTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);//Guru
                                }
                                amTotalDistance += touchHandlingDistance;
                                anytimeTotalDistance += touchHandlingDistance;//Guru
                            }
                            else if (time == "PM")
                            {
                                if (!OverrideTouchScheduling(dsTouchTypes, Convert.ToInt32(drTouch["TouchTypeId"])))
                                {
                                    pmTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);
                                    anytimeTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);//Guru
                                }
                                pmTotalDistance += touchHandlingDistance;
                                anytimeTotalDistance += touchHandlingDistance;//Guru
                            }
                            else if (time == "ANYTIME")
                            {
                                if (!OverrideTouchScheduling(dsTouchTypes, Convert.ToInt32(drTouch["TouchTypeId"])))
                                    anytimeTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);
                                anytimeTotalDistance += touchHandlingDistance;
                            }
                        }
                        else
                        {
                            if (time == "AM")
                            {
                                if (!OverrideTouchScheduling(dsTouchTypes, Convert.ToInt32(drTouch["TouchTypeId"])))
                                {
                                    amOtherTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);
                                    anytimeOtherTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);//Guru
                                }
                                amOtherTotalDistance += touchHandlingDistance;
                                anytimeOtherTotalDistance += touchHandlingDistance;//Guru
                            }
                            else if (time == "PM")
                            {
                                if (!OverrideTouchScheduling(dsTouchTypes, Convert.ToInt32(drTouch["TouchTypeId"])))
                                {
                                    pmOtherTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);
                                    anytimeOtherTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);//Guru
                                }
                                pmOtherTotalDistance += touchHandlingDistance;
                                anytimeOtherTotalDistance += touchHandlingDistance;//Guru
                            }
                            else if (time == "ANYTIME")
                            {
                                if (!OverrideTouchScheduling(dsTouchTypes, Convert.ToInt32(drTouch["TouchTypeId"])))
                                    anytimeOtherTotalDistance += Convert.ToDecimal(drTouch["TouchMiles"]);
                                anytimeOtherTotalDistance += touchHandlingDistance;
                            }
                        }
                    }
                }
                #endregion /Loop through Sitelink touches for scheduled day and accum touch buckets

                #region Update availability columns for Schedule Calendar Tables

                // Old logic of determining holidays 
                if (dsCapacity.Tables[0].Rows.OfType<DataRow>().Any(c => Convert.ToDateTime(c["CapacityDate"]) == dtSchedule && Convert.ToString(c["StoreOpen"]) == "O"))
                {
                    amRow.StoreStatus = "O";
                    pmRow.StoreStatus = "O";
                    anyTimeRow.StoreStatus = "O";
                }
                else
                {
                    amRow.StoreStatus = "C";
                    pmRow.StoreStatus = "C";
                    anyTimeRow.StoreStatus = "C";
                }

                #endregion /Update availability columns for Schedule Calendar Tables

                #region Get capacity details

                // Calculations

                DataRow drCapacity = dsCapacity.Tables[0].Rows.OfType<DataRow>().FirstOrDefault(c => Convert.ToDateTime(c["CapacityDate"]) == dtSchedule && Convert.ToInt32(c["CategoryId"]) == CAPCategoryId);

                bool IBOAndNoContainer = IsTouchTypeIBOAndNoContainer(drCapacity, containerSize.Value, dsTouchTypes, touchTypeId.Value);

                // GET TOTAL SUM OF TIER RESERVE MILES WITH IN THE TIER
                decimal sumMinReserves = dsCapacity.Tables[0].Rows.OfType<DataRow>().Where(c => Convert.ToDateTime(c["CapacityDate"]) == dtSchedule &&
                    Convert.ToInt32(c["Tier"]) == Convert.ToInt32(drCapacity["Tier"]) &&
                    Convert.ToString(c["ReserveType"]) == "TIER"
                    ).Sum(c => Convert.ToDecimal(c["MinReserve"]));

                // SET THE BOOKED MILEAGES FOR ANYTIME.
                // SETTING UP THE BOOKED MILES IS BIT OF TRICKY
                // IF WE HAVE MORE THAN ONE ALLOCATION IN THE SAME TIER WHICH THE CURRENT ALLOCATION BELONGS TO....
                // BOOKED MILES ARE THE TOTAL RAW BOOKED MILES COMING FROM SITELINK PLUS TOTAL REMAINING RESERVED MILES FROM OTHER ALLOCATIONS IN THE SAME TIER

                // CALCULATE TOTAL REMAINING RESERVED MILES

                // GET OTHER CAPACITIES IN THE SAME TIER - IN THIS CASE IT IS SERVICE/SALES

                DataRow drOtherCapacity = dsCapacity.Tables[0].Rows.OfType<DataRow>().FirstOrDefault(c => Convert.ToDateTime(c["CapacityDate"]) == dtSchedule && Convert.ToInt32(c["CategoryId"]) != CAPCategoryId && Convert.ToInt32(c["Tier"]) == Convert.ToInt32(drCapacity["Tier"]));

                // TOTAL RAW BOOKINGS FROM SITELINK
                decimal totalRawBookingSL = anytimeOtherTotalDistance + anytimeTotalDistance;
                decimal otherRegularMiles = CalculateRegularMiles(drOtherCapacity, sumMinReserves, totalRawBookingSL, anytimeOtherTotalDistance);
                decimal otherReserveMiles = CalculateReservedMiles(drOtherCapacity, otherRegularMiles);

                // NOW THE ANYTIME BOOKED MILES ARE THE TOTAL RAW BOOKINGS FROM SITELINK
                anyTimeRow.BookedMiles = anytimeTotalDistance;

                // SET THE FACILITY MILEAGES FOR ANYTIME.
                anyTimeRow.FacilityMiles = drCapacity["TotalAnyTimeMileage"] == DBNull.Value ? 0M : Convert.ToDecimal(drCapacity["TotalAnyTimeMileage"]);

                // SET THE FULL FACILITY MILEAGES FOR ANYTIME.
                anyTimeRow.FullFacilityMiles = drCapacity["TotalFacilityTruckAnyTimeMileage"] == DBNull.Value ? 0M : Convert.ToDecimal(drCapacity["TotalFacilityTruckAnyTimeMileage"]);

                // SET THE TRUCKS IN REPAIR.
                anyTimeRow.TrucksInRepair = drCapacity["TrucksInRepair"] == DBNull.Value ? 0 : Convert.ToInt32(drCapacity["TrucksInRepair"]);

                // SET THE TRUCKS IN HOUR CHANGE.
                anyTimeRow.TrucksInHourChange = drCapacity["TrucksInHourChange"] == DBNull.Value ? 0 : Convert.ToInt32(drCapacity["TrucksInHourChange"]);

                // SET THE TRUCKS IN OM.
                anyTimeRow.TrucksInOM = drCapacity["TrucksInOM"] == DBNull.Value ? 0 : Convert.ToInt32(drCapacity["TrucksInOM"]);

                // SET THE REGULAR MILEAGES FOR ANYTIME.

                anyTimeRow.RegularMiles = CalculateRegularMiles(drCapacity, sumMinReserves, totalRawBookingSL, anytimeTotalDistance);

                // SET THE RESERVED MILES
                anyTimeRow.ReservedMiles = CalculateReservedMiles(drCapacity, anyTimeRow.RegularMiles);

                // SET SALES & SERVICE REGULAR MILES / RESERVE MILES
                if (category.ToLower() == "sales")
                {
                    anyTimeRow.SalesRegularMiles = anyTimeRow.RegularMiles;
                    anyTimeRow.ServiceRegularMiles = otherRegularMiles;
                    // SEND REGULAR CAPACITY MORE THAN 0 TO GET FULL RESERVE
                    anyTimeRow.SalesReservedMiles = CalculateReservedMiles(drCapacity, 1);
                    // SEND REGULAR CAPACITY MORE THAN 0 TO GET FULL RESERVE
                    anyTimeRow.ServiceReservedMiles = CalculateReservedMiles(drOtherCapacity, 1);
                    anyTimeRow.SalesBookedMiles = anytimeTotalDistance;
                    anyTimeRow.ServiceBookedMiles = anytimeOtherTotalDistance;
                }
                else
                {
                    anyTimeRow.ServiceRegularMiles = anyTimeRow.RegularMiles;
                    anyTimeRow.SalesRegularMiles = otherRegularMiles;
                    // SEND REGULAR CAPACITY MORE THAN 0 TO GET FULL RESERVE
                    anyTimeRow.SalesReservedMiles = CalculateReservedMiles(drOtherCapacity, 1);
                    // SEND REGULAR CAPACITY MORE THAN 0 TO GET FULL RESERVE
                    anyTimeRow.ServiceReservedMiles = CalculateReservedMiles(drCapacity, 1);
                    anyTimeRow.SalesBookedMiles = anytimeOtherTotalDistance;
                    anyTimeRow.ServiceBookedMiles = anytimeTotalDistance;
                }

                // POPULATE COLOR CODE & ADJUST MILES

                // ANYTIME
                anyTimeRow.BookedMiles += totalDistance;
                anyTimeRow.RegularMiles -= totalDistance;

                SetColor(anyTimeRow, drCapacity, containerSize, totalDistance, dtSchedule, limitDaysTo, "sales", touchDistance, overrideTouchSchedule, IBOAndNoContainer);
                SetColor(anyTimeRow, drCapacity, containerSize, totalDistance, dtSchedule, limitDaysTo, "service", touchDistance, overrideTouchSchedule, IBOAndNoContainer);
                SetColor(anyTimeRow, drCapacity, containerSize, totalDistance, dtSchedule, limitDaysTo, "current", touchDistance, overrideTouchSchedule, IBOAndNoContainer);

                anyTimeRow.BookedMiles += anytimeOtherTotalDistance;
                #endregion


                // ************************************************** AM readings ************************************************
                amRow.FacilityMiles = drCapacity["TotalAMMileage"] == DBNull.Value ? 0M : Convert.ToDecimal(drCapacity["TotalAMMileage"]);
                amRow.BookedMiles = amTotalDistance + amOtherTotalDistance + totalDistance;
                amRow.RegularMiles = amRow.FacilityMiles - amRow.BookedMiles;

                amRow.SalesRegularMiles = 0;
                amRow.ServiceRegularMiles = 0;
                // EFFECTIVE MILES LEFT IN ANYTIME
                var milesLeft = anyTimeRow.RegularMiles + anyTimeRow.SalesReservedMiles + anyTimeRow.ServiceReservedMiles;
                amRow.RegularMiles = amRow.RegularMiles > milesLeft ? milesLeft : amRow.RegularMiles;
                // SET THE RESERVED MILES
                if (Convert.ToString(drCapacity["ReserveType"]) == "GBL")
                    amRow.ReservedMiles = Convert.ToDecimal(drCapacity["MinReserve"]) * Convert.ToDecimal(drCapacity["TotalAMMileage"]);
                else
                    amRow.ReservedMiles = Convert.ToDecimal(drCapacity["MinReserve"]) * Convert.ToDecimal(drCapacity["AvailableMileage"]);

                // SET AM COLOR
                if (limitDaysTo < 0 || dtSchedule > DateTime.Today.AddDays(limitDaysTo))
                {
                    if (amRow.StoreStatus == "O")
                    {
                        // FOR THOSE TOUCHES WHICH DO NO HAVE TOUCH TIMES SPECIFIED
                        // WILL BYPASS CAPACITY VALIDATION
                        if (touchDistance <= 0 && overrideTouchSchedule)
                            if (IBOAndNoContainer)
                                amRow.ColorCode = Convert.ToInt16(ColorCodes.Orange); // Orange
                            else
                                amRow.ColorCode = Convert.ToInt16(ColorCodes.Green); // GREEN
                        else
                        {
                            // CASE WHEN THERE ARE NO REGULAR MILES
                            if (amRow.RegularMiles <= 0)
                            {
                                // THERE IS NO CASE OF RED MILES WHEN IT IS AM/PM
                                amRow.ColorCode = Convert.ToInt16(ColorCodes.Black); // BLACK 
                            }
                            else
                            {
                                amRow.ColorCode = anyTimeRow.ColorCode;
                            }
                        }
                    }
                    else
                        amRow.ColorCode = Convert.ToInt16(ColorCodes.Blue); // BLUE                
                }
                else
                    amRow.ColorCode = Convert.ToInt16(ColorCodes.White); // WHITE

                // ************************************************** PM readings ************************************************

                pmRow.FacilityMiles = drCapacity["TotalPMMileage"] == DBNull.Value ? 0M : Convert.ToDecimal(drCapacity["TotalPMMileage"]);
                pmRow.BookedMiles = pmTotalDistance + pmOtherTotalDistance + totalDistance;
                pmRow.RegularMiles = pmRow.FacilityMiles - pmRow.BookedMiles;

                pmRow.SalesRegularMiles = 0;
                pmRow.ServiceRegularMiles = 0;

                pmRow.RegularMiles = pmRow.RegularMiles > milesLeft ? milesLeft : pmRow.RegularMiles;
                // SET THE RESERVED MILES

                if (Convert.ToString(drCapacity["ReserveType"]) == "GBL")
                    pmRow.ReservedMiles = Convert.ToDecimal(drCapacity["MinReserve"]) * Convert.ToDecimal(drCapacity["TotalPMMileage"]);
                else
                    pmRow.ReservedMiles = Convert.ToDecimal(drCapacity["MinReserve"]) * Convert.ToDecimal(drCapacity["AvailableMileage"]);

                // SET PM COLOR
                if (limitDaysTo < 0 || dtSchedule > DateTime.Today.AddDays(limitDaysTo))
                {
                    if (pmRow.StoreStatus == "O")
                    {
                        // FOR THOSE TOUCHES WHICH DO NO HAVE TOUCH TIMES SPECIFIED
                        // WILL BYPASS CAPACITY VALIDATION
                        if (touchDistance <= 0 && overrideTouchSchedule)
                            if (IBOAndNoContainer)
                                pmRow.ColorCode = Convert.ToInt16(ColorCodes.Orange); // Orange
                            else
                                pmRow.ColorCode = Convert.ToInt16(ColorCodes.Green); // GREEN
                        else
                        {
                            // CASE WHEN THERE ARE NO REGULAR MILES
                            if (pmRow.RegularMiles <= 0)
                            {
                                // THERE IS NO CASE OF RED MILES WHEN IT IS AM/PM
                                pmRow.ColorCode = Convert.ToInt16(ColorCodes.Black); // BLACK 
                            }
                            else
                            {
                                pmRow.ColorCode = anyTimeRow.ColorCode;
                            }
                        }
                    }
                    else
                        pmRow.ColorCode = Convert.ToInt16(ColorCodes.Blue); // BLUE         
                }
                else
                    pmRow.ColorCode = Convert.ToInt16(ColorCodes.White); // BLUE         

                amCalendar.AddAMRow(amRow);
                pmCalendar.AddPMRow(pmRow);
                anytimeCalendar.AddAnyTimeRow(anyTimeRow);
            }
            #endregion /Loop each Schedule day and determine Touch availability for each scheduled date.

            #region Add trip miles to booked mileages & available/regular mileages
            scheduleCalendar.AnyTime.OfType<ScheduleCalendar.AnyTimeRow>().ToList().ForEach(r =>
            {
                r.BookedMiles -= r.TripMiles;
                r.RegularMiles += r.TripMiles;

                // LATEST CHANGE : RESERVE MILES ARE TOTAL RESERVES OF BOTH SALES AND SERVICE
                if (r.RegularMiles < 0)
                    r.ReservedMiles = r.ServiceReservedMiles + r.SalesReservedMiles + r.RegularMiles;
                else
                    r.ReservedMiles = r.ServiceReservedMiles + r.SalesReservedMiles;

                // Make regular miles look zero when it is negative
                if (r.RegularMiles < 0)
                    r.RegularMiles = 0;
                if (r.ReservedMiles < 0)
                    r.ReservedMiles = 0;
                if (r.SalesRegularMiles < 0)
                    r.SalesRegularMiles = 0;
                if (r.ServiceRegularMiles < 0)
                    r.ServiceRegularMiles = 0;

                r.BookedMiles = r.ServiceBookedMiles + r.SalesBookedMiles;
            });

            scheduleCalendar.AM.OfType<ScheduleCalendar.AMRow>().ToList().ForEach(r =>
            {
                r.BookedMiles -= r.TripMiles;
                r.RegularMiles += r.TripMiles;
                // Make regular miles look zero when it is negative
                if (r.RegularMiles < 0)
                    r.RegularMiles = 0;
                if (r.ReservedMiles < 0)
                    r.ReservedMiles = 0;
                if (r.SalesRegularMiles < 0)
                    r.SalesRegularMiles = 0;
                if (r.ServiceRegularMiles < 0)
                    r.ServiceRegularMiles = 0;
            });

            scheduleCalendar.PM.OfType<ScheduleCalendar.PMRow>().ToList().ForEach(r =>
            {
                r.BookedMiles -= r.TripMiles;
                r.RegularMiles += r.TripMiles;

                // Make regular miles look zero when it is negative
                if (r.RegularMiles < 0)
                    r.RegularMiles = 0;
                if (r.ReservedMiles < 0)
                    r.ReservedMiles = 0;
                if (r.SalesRegularMiles < 0)
                    r.SalesRegularMiles = 0;
                if (r.ServiceRegularMiles < 0)
                    r.ServiceRegularMiles = 0;
            });

            #endregion

            #region Case when anytime facility miles are less than AM facility than set AM mileages to anytime mileages

            scheduleCalendar.AM.OfType<ScheduleCalendar.AMRow>().ToList().ForEach(r =>
            {
                var anyTimeFacilityMiles = scheduleCalendar.AnyTime.OfType<ScheduleCalendar.AnyTimeRow>().FirstOrDefault(c => c.Date == r.Date).FacilityMiles;
                if (anyTimeFacilityMiles < r.FacilityMiles)
                    r.FacilityMiles = anyTimeFacilityMiles;
            });

            #endregion

            DataSet dsCapacityData = GetScheduleDataTable(scheduleCalendar, locationCode);
              
            return dsCapacityData;
        }

        private void SetColor(ScheduleCalendar.AnyTimeRow anyTimeRow, DataRow drCapacity, int? containerSize, decimal totalDistance, DateTime dtSchedule, int limitDaysTo, string allocation, decimal touchDistance, bool overrideTouchScheduling, bool IBOAndNoContainer)
        {
            decimal regularMiles = 0;
            decimal reservedMiles = 0;
            short colorCode = -1;
            reservedMiles = anyTimeRow.SalesReservedMiles + anyTimeRow.ServiceReservedMiles;

            if (allocation == "sales")
            {
                regularMiles = anyTimeRow.SalesRegularMiles - totalDistance;
            }
            else if (allocation == "service")
            {
                regularMiles = anyTimeRow.ServiceRegularMiles - totalDistance;
            }
            else
            {
                regularMiles = anyTimeRow.RegularMiles;
            }
            if (limitDaysTo < 0 || dtSchedule > DateTime.Today.AddDays(limitDaysTo))
            {
                if (anyTimeRow.StoreStatus == "O")
                {
                    // FOR THOSE TOUCHES WHICH DO NO HAVE TOUCH TIMES SPECIFIED
                    // WILL BYPASS CAPACITY VALIDATION
                    if (touchDistance <= 0 && overrideTouchScheduling)
                    {
                        if (IBOAndNoContainer)
                            colorCode = Convert.ToInt16(ColorCodes.Orange); // ORANGE
                        else
                            colorCode = Convert.ToInt16(ColorCodes.Green); // GREEN
                    }
                    else
                    {
                        // CASE WHEN THERE ARE NO REGULAR MILES
                        if (regularMiles <= 0)
                        {
                            // CASE WHEN THERE ARE NO RED MILES
                            if ((reservedMiles + regularMiles) < 0)
                            {

                                colorCode = Convert.ToInt16(ColorCodes.Black); // BLACK
                            }
                            else
                            {
                                // CHECK IF THE UNIT IS AVAILABLE
                                if ((Convert.ToBoolean(drCapacity["Is8FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.EightFeet) ||
                                    (Convert.ToBoolean(drCapacity["Is12FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.TweleveFeet) ||
                                    (Convert.ToBoolean(drCapacity["Is16FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.SixteenFeet))
                                    colorCode = Convert.ToInt16(ColorCodes.Red); // RED
                                else
                                    colorCode = Convert.ToInt16(ColorCodes.Purple); // REDORANGE 
                            }
                        }
                        else
                        {
                            // CHECK IF THE UNIT IS AVAILABLE
                            if ((Convert.ToBoolean(drCapacity["Is8FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.EightFeet) ||
                                (Convert.ToBoolean(drCapacity["Is12FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.TweleveFeet) ||
                                (Convert.ToBoolean(drCapacity["Is16FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.SixteenFeet))
                                colorCode = Convert.ToInt16(ColorCodes.Green); // GREEN
                            else
                                colorCode = Convert.ToInt16(ColorCodes.Orange); // GREENORANGE

                        }
                    }
                }
                else
                    colorCode = Convert.ToInt16(ColorCodes.Blue); // BLUE 
            }
            else
                colorCode = Convert.ToInt16(ColorCodes.White); // WHITE 
            if (allocation == "sales")
                anyTimeRow.SalesColorCode = colorCode;
            else if (allocation == "service")
                anyTimeRow.ServiceColorCode = colorCode;
            else
                anyTimeRow.ColorCode = colorCode;
        }

        private decimal CalculateReservedMiles(DataRow drCapacity, decimal regularMiles)
        {
            decimal reservedMiles;
            decimal reserveMax;
            if (Convert.ToString(drCapacity["ReserveType"]) == "GBL")
                reserveMax = Convert.ToDecimal(drCapacity["MinReserve"]) * Convert.ToDecimal(drCapacity["TotalAnyTimeMileage"]);
            else
                reserveMax = Convert.ToDecimal(drCapacity["MinReserve"]) * Convert.ToDecimal(drCapacity["AvailableMileage"]);
            if (regularMiles < 0)
                reservedMiles = regularMiles + reserveMax;
            else
                reservedMiles = reserveMax;
            return reservedMiles;
        }

        private decimal CalculateRegularMiles(DataRow drCapacity, decimal sumMinReserves, decimal totalRawBookingSL, decimal relatedRawBookingSL)
        {
            // CALCULATING REGULAR MILES 
            // NORMALLY REGULAR MILES ARE MAXPERCENT TIMES (*) AVAILBLE CAPACITY IF RESERVE TYPE IS GLOBAL 
            // AND IT IS (MAXPERCENT - MINPERCENT) TIMES (*) AVAILABLE CAPACITY IF RESERVE TYPE IS TIER
            // HOWEVER IT IS DIFFERENT IF THE REGULAR MILE RESULT IS GOING BEYOND THE MAX VALUE 
            // MAX VALUE FOR THE REGULAR MILE IS :
            // CASE WHEN RESERVE TYPE IS GLOBAL = RESERVES ARE ALREADY TAKEN OFF SO AVAILBLE MILEAGES WILL INCLUDE TIER LEVEL RESERVES
            // MAX VALUE WILL BE : AVAILABLE - TOTAL BOOKED MILES IN THE ENTIRE TIER
            // CASE WHEN RESERVE TYPE IS TIER = RESERVES ARE NO TAKEN OFF SO AVAILBLE MILEAGES WILL NOT INCLUDE TIER LEVEL RESERVES
            // MAX VALUE WILL BE : AVAILABLE - TOTAL RESERVES IN THE ENTIRE TIER - TOTAL BOOKED MILES IN THE ENTIRE TIER

            decimal regularMiles = 0M;
            decimal anyTimeMax1 = 0M;
            decimal anyTimeMax2 = (1 - sumMinReserves) * Convert.ToDecimal(drCapacity["AvailableMileage"]);


            if (Convert.ToString(drCapacity["ReserveType"]) == "GBL")
                anyTimeMax1 = (Convert.ToDecimal(drCapacity["Maximum"]) * Convert.ToDecimal(drCapacity["AvailableMileage"]));
            else
                anyTimeMax1 = (Convert.ToDecimal(drCapacity["Maximum"]) - Convert.ToDecimal(drCapacity["MinReserve"])) * Convert.ToDecimal(drCapacity["AvailableMileage"]);

            if (anyTimeMax1 > anyTimeMax2)
                regularMiles = anyTimeMax2;
            else
                regularMiles = anyTimeMax1;

            decimal remainingTotalRegularMiles = anyTimeMax2 - totalRawBookingSL;
            decimal remainingRelatedRegularMiles = regularMiles - relatedRawBookingSL;

            // BELOW CONDITION IS CHECKED TO MAINTAIN THE REGULAR MILES OF A CURRENTLY CALCULATED ALLOCATION (CCA)
            // CCA's REGULAR MILES WILL BE MAINTAINED TILL THE TIME IT IS BELOW THE TOTAL REGULAR MILES OF A TIER
            // IT THAT VALUE GOES ABOVE THE TOTAL REGULAR MILES OF THE TIER CCA's REGULAR MILES WILL BE MADE EQUAL TO TOTAL REGULAR MILES OF A TIER
            if (remainingRelatedRegularMiles > remainingTotalRegularMiles)
                regularMiles = remainingTotalRegularMiles;
            else
                regularMiles = remainingRelatedRegularMiles;

            return regularMiles;
        }

        private DataSet GetAllCapTouchTypes()
        {
            LocalComponent lc = new LocalComponent(_connStringPR);
            return lc.GetAllCapTouchTypes();
        }

        private decimal GetTouchMilesByTouchTypeId(int touchTypeId, DateTime date, string locationCode)
        {
            decimal distance = 0;
            LocalComponent local = new LocalComponent(_connStringPR);
            DataSet dsTouchMiles = local.GetTouchMilesByTypeByFacilityByDateRange(touchTypeId, locationCode, date, date);
            var miles = dsTouchMiles.Tables[0].Rows.OfType<DataRow>().Select(c => Convert.ToDecimal(c["TouchMiles"])).FirstOrDefault();

            return miles > 0 ? miles : distance;
        }

        /// <summary>
        /// IsTouchTypeIBOAndNoContainer Method returns true when the given touch is IBO and there is no availability of given container size
        /// </summary>
        /// <param name="drCapacity"></param>
        /// <param name="containerSize"></param>
        /// <param name="dsTouchTypes"></param>
        /// <param name="touchTypeId"></param>
        /// <returns></returns>
        private bool IsTouchTypeIBOAndNoContainer(DataRow drCapacity, int containerSize, DataSet dsTouchTypes, int touchTypeId)
        {
            if ((Convert.ToBoolean(drCapacity["Is8FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.EightFeet) ||
                (Convert.ToBoolean(drCapacity["Is12FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.TweleveFeet) ||
                (Convert.ToBoolean(drCapacity["Is16FootUnitAvailable"]) && containerSize == (int)UnitOrContainerSize.SixteenFeet))
                return dsTouchTypes.Tables[0].Rows.OfType<DataRow>().Any(t => Convert.ToInt32(t["CAPTouchTypeId"]) == touchTypeId && Convert.ToString(t["TouchTypeCode"]) == "IBO");
            else
                return false;
        }

        private bool OverrideTouchScheduling(DataSet dsTouchTypes, int touchTypeId)
        {
            return dsTouchTypes.Tables[0].Rows.OfType<DataRow>().Any(t => Convert.ToInt32(t["CAPTouchTypeId"]) == touchTypeId && Convert.ToString(t["OverrideScheduling"]) == "True");
        }

        #region Methods to get Capacity and save data DB for TG-450

        private DataSet GetScheduleDataTable(ScheduleCalendar scheduleCalendar, string locationCode)
        {
            var dataSetAnytime = scheduleCalendar.AnyTime.Select(
                  x => new PR.Entities.TouchLogDetail()
                  {
                      CAPDate = x["Date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(x["Date"]),
                      IsAvailable = x["IsAvailable"] == DBNull.Value ? false : Convert.ToBoolean(x["IsAvailable"]),
                      StoreStatus = x["StoreStatus"] == DBNull.Value ? string.Empty : x["StoreStatus"].ToString(),
                      TouchesScheduled = x["TouchesScheduled"] == DBNull.Value ? default(int) : Convert.ToInt32(x["TouchesScheduled"]),
                      RegularMiles = x["SalesRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["SalesRegularMiles"]),
                      ReservedMiles = x["ReservedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["ReservedMiles"]),
                      FacilityMiles = x["FacilityMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["FacilityMiles"]),
                      TripMiles = x["TripMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["TripMiles"]),
                      BookedMiles = x["BookedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["BookedMiles"]),
                      ColorCode = x["ColorCode"] == DBNull.Value ? default(int) : Convert.ToInt32(x["ColorCode"]),
                      TouchLimit = x["TouchLimit"] == DBNull.Value ? default(int) : Convert.ToInt32(x["TouchLimit"]),
                      MileagesBooked = x["MileagesBooked"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["MileagesBooked"]),
                      SalesRegularMiles = x["SalesRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["SalesRegularMiles"]),
                      ServiceRegularMiles = x["ServiceRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["ServiceRegularMiles"]),
                      MileageLimit = x["MileageLimit"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["MileageLimit"]),
                      TouchTime = "Anytime",

                      TrucksInHourChange = x["TrucksInHourChange"] == DBNull.Value ? default(int) : Convert.ToInt32((x["TrucksInHourChange"])),
                      TrucksInOM = x["TrucksInOM"] == DBNull.Value ? default(int) : Convert.ToInt32(x["TrucksInOM"]),
                      TrucksInRepair = x["TrucksInRepair"] == DBNull.Value ? default(int) : Convert.ToInt32(x["TrucksInRepair"]),
                      ServiceReservedMiles = x["ServiceReservedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["ServiceReservedMiles"]),
                      SalesReservedMiles = x["SalesReservedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["SalesReservedMiles"]),
                      DowntimeMiles = x["DowntimeMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["DowntimeMiles"]),
                      FullFacilityMiles = x["FullFacilityMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["FullFacilityMiles"]),
                      ServiceColorCode = x["ServiceColorCode"] == DBNull.Value ? default(int) : Convert.ToInt32(x["ServiceColorCode"]),
                      SalesColorCode = x["SalesColorCode"] == DBNull.Value ? default(int) : Convert.ToInt32(x["SalesColorCode"]),
                      SalesBookedMiles = x["SalesBookedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["SalesBookedMiles"]),
                      ServiceBookedMiles = x["ServiceBookedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["ServiceBookedMiles"])

                  }).ToList();

            var dataSetAM = scheduleCalendar.AM.Select(
               x => new PR.Entities.TouchLogDetail()
               {
                   CAPDate = x["Date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(x["Date"]),
                   IsAvailable = x["IsAvailable"] == DBNull.Value ? false : Convert.ToBoolean(x["IsAvailable"]),
                   StoreStatus = x["StoreStatus"] == DBNull.Value ? string.Empty : x["StoreStatus"].ToString(),
                   TouchesScheduled = x["TouchesScheduled"] == DBNull.Value ? default(int) : Convert.ToInt32(x["TouchesScheduled"]),
                   RegularMiles = x["SalesRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["SalesRegularMiles"]),
                   ReservedMiles = x["ReservedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["ReservedMiles"]),
                   FacilityMiles = x["FacilityMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["FacilityMiles"]),
                   TripMiles = x["TripMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["TripMiles"]),
                   BookedMiles = x["BookedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["BookedMiles"]),
                   ColorCode = x["ColorCode"] == DBNull.Value ? default(int) : Convert.ToInt32(x["ColorCode"]),
                   TouchLimit = x["TouchLimit"] == DBNull.Value ? default(int) : Convert.ToInt32(x["TouchLimit"]),
                   MileagesBooked = x["MileagesBooked"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["MileagesBooked"]),
                   SalesRegularMiles = x["SalesRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["SalesRegularMiles"]),
                   ServiceRegularMiles = x["ServiceRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["ServiceRegularMiles"]),
                   MileageLimit = x["MileageLimit"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["MileageLimit"]),
                   TouchTime = "AM",
               }).ToList();

            var dataSetPM = scheduleCalendar.PM.Select(
              x => new PR.Entities.TouchLogDetail()
              {
                  CAPDate = x["Date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(x["Date"]),
                  IsAvailable = x["IsAvailable"] == DBNull.Value ? false : Convert.ToBoolean(x["IsAvailable"]),
                  StoreStatus = x["StoreStatus"] == DBNull.Value ? string.Empty : x["StoreStatus"].ToString(),
                  TouchesScheduled = x["TouchesScheduled"] == DBNull.Value ? default(int) : Convert.ToInt32(x["TouchesScheduled"]),
                  RegularMiles = x["SalesRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["SalesRegularMiles"]),
                  ReservedMiles = x["ReservedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["ReservedMiles"]),
                  FacilityMiles = x["FacilityMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["FacilityMiles"]),
                  TripMiles = x["TripMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["TripMiles"]),
                  BookedMiles = x["BookedMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["BookedMiles"]),
                  ColorCode = x["ColorCode"] == DBNull.Value ? default(int) : Convert.ToInt32(x["ColorCode"]),
                  TouchLimit = x["TouchLimit"] == DBNull.Value ? default(int) : Convert.ToInt32(x["TouchLimit"]),
                  MileagesBooked = x["MileagesBooked"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["MileagesBooked"]),
                  SalesRegularMiles = x["SalesRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["SalesRegularMiles"]),
                  ServiceRegularMiles = x["ServiceRegularMiles"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["ServiceRegularMiles"]),
                  MileageLimit = x["MileageLimit"] == DBNull.Value ? default(decimal) : Convert.ToDecimal(x["MileageLimit"]),
                  TouchTime = "PM",
              }).ToList();

            var fullCalCap = dataSetAnytime.Union(dataSetAM).Union(dataSetPM).ToList();

            DataTable capTouchMilesDataTable = ConvertToDataTable(fullCalCap);

            //Add location code values and column in return data result
            capTouchMilesDataTable.Columns.Add("LocationCode", typeof(System.String));
            foreach (DataRow dr in capTouchMilesDataTable.Rows) { dr["LocationCode"] = locationCode; }

            //Get Facility Details along with Color Codes from Database.
            DataSet dsCapacityData = GetCapacityCalendarData(capTouchMilesDataTable);

            return dsCapacityData;
        }

        private DataSet GetCapacityCalendarData(DataTable capTouchMilesDataTable)
        {
            LocalComponent local = new LocalComponent(_connStringPR);
            DataSet dsCapacityData = local.GetCapacityCalendarData(capTouchMilesDataTable);
            dsCapacityData.Tables[0].TableName = "CapacityData";
            return dsCapacityData;
        }

        private DataTable ConvertToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            System.Reflection.PropertyInfo[] Props = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            foreach (System.Reflection.PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable

            dataTable.TableName = "CapacityData";
            return dataTable;
        }

        #endregion

    }
}
