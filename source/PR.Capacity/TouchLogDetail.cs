﻿using System;

namespace PR.Entities
{
    public class TouchLogDetail
    { 
        public string TouchTime { get; set; }

        public DateTime CAPDate { get; set; }

        public bool IsAvailable { get; set; }

        public string StoreStatus { get; set; }

        public int TrucksInHourChange { get; set; }

        public int TrucksInOM { get; set; }

        public int TrucksInRepair { get; set; }

        public int TouchesScheduled { get; set; }

        public decimal RegularMiles { get; set; }

        public decimal SalesRegularMiles { get; set; }

        public decimal ServiceRegularMiles { get; set; }

        public decimal ServiceReservedMiles { get; set; }

        public decimal SalesReservedMiles { get; set; }

        public decimal ReservedMiles { get; set; }

        public decimal FacilityMiles { get; set; }

        public decimal TripMiles { get; set; }

        public decimal DowntimeMiles { get; set; }
         
        public decimal SalesBookedMiles { get; set; }

        public decimal ServiceBookedMiles { get; set; }

        public decimal BookedMiles { get; set; }

        public int ServiceColorCode { get; set; }

        public int SalesColorCode { get; set; }

        public int ColorCode { get; set; }

        public int TouchLimit { get; set; }

        public decimal MileagesBooked { get; set; }

        public decimal FullFacilityMiles { get; set; }

        public decimal MileageLimit { get; set; } 

    }
}

